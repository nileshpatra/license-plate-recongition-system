# License Plate Recongition System
## Version: 0.2

* This Project is an implementation of license detection and number plate digit recognition system. When an image is passed through this, the
  license area is filtered out and the digits on the license are detected and printed onto the terminal.

* Function of various files is as follows:
```
	├── `src` - contains the source code of the module
	    |──  `runanalysis.py` - Main file to be run for prediction.
	│   ├── `extract_and_recognize.py` - Helper file to be run. Extracts and predicts the the license plate numbers and alphabets
	│   ├── `filter_and_recongize_plate.py` - Given a cropped input, Pre-processes + Loads the ML model for predicting Numbers/alphabets + predicts the output
	│   ├── `__init__.py` - Blank file for the sake of import issues
	│   ├── `numerals_detection_model.py` - Model for predicting numbers/alphabets from image is contained
	│   └── `plate_detection_model.py` - Image Processsing techniques for extracting number plate from the given image
	├── `model_data` - Datasets and saved model parameters
    ├── `requirements.txt` - All `pip-installable` requirements are listed

    └── `tests` - CI tests for confirming the integrity of the code
```

* This also is accompanied by a GUI version which looks as follows:
![Corresponding GUI Image](assets/licensedetectgui.png)

## Installing:
```
	git clone https://gitlab.com/git-boi/license-plate-recongition-system.git
	cd license-plate-recongition-system
	python3 -m pip install -r requirements.txt
	python3 -m pip install kivy kivy_examples --pre --extra-index-url https://kivy.org/downloads/simple/
```

## Running:

### CLI Version:
```
	python3 -m src.runanalysis --image /path/to/image/file
```

### GUI Version:
```
	python3 -m gui.gui
```
