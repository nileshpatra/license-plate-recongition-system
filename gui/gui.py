import sys
from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.checkbox import CheckBox
import src.extract_and_recognize


class LicensePrint(GridLayout):

    def __init__(self, **kwargs):
        super(LicensePrint, self).__init__(**kwargs)
        self.cols = 2

        # Label and button for filepath
        self.add_widget(Label(text='Path to Image File'))
        self.filepath = TextInput(multiline=False)
        self.add_widget(self.filepath)

        # Label and button for output
        self.add_widget(Label(text='Predicted Number:'))
        self.licensenumber = TextInput(multiline=False)
        self.add_widget(self.licensenumber)

        # Checkbox for enabling wpod
        self.add_widget(Label(text='Enable wpod'))
        self.wpod_enable = CheckBox()
        self.add_widget(self.wpod_enable)

        # Buttons for starting and stopping operation
        self.startbutton = Button(text='Start', font_size=14)
        self.stopbutton = Button(text='Stop', font_size=14)
        self.startbutton.bind(on_press=self.startprediction)
        self.stopbutton.bind(on_press=self.stopprediction)
        self.add_widget(self.startbutton)
        self.add_widget(self.stopbutton)

    def startprediction(self, instance):
        print("prediction started")
        self.licensenumber.text = "Processing ........."
        if self.wpod_enable.active:
            platenum = src.extract_and_recognize.main(self.filepath.text, True)
        else:
            platenum = src.extract_and_recognize.main(self.filepath.text)
        self.licensenumber.text = platenum
        print('DONE!')

    def stopprediction(self, instance):
        sys.exit(0)


class LicenseDetectApp(App):

    def build(self):
        return LicensePrint()


if __name__ == '__main__':
    LicenseDetectApp().run()
