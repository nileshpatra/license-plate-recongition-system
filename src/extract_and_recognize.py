from src.plate_detection_model import *
import src.filter_and_recongize_plate
from src.wpod_plate_detection_model import *
import matplotlib.image as mpimg
import cv2
import numpy
import os
import glob
import sys


def extract_plate(input_image):
    input_image = cv2.imread(input_image)
    detection, crops = detect(input_image)

    i = 1
    for crop in crops:
        crop = process(crop)
        cv2.imwrite('temp/crop' + str(i) + '.jpg', crop)
        i += 1
    cv2.imwrite('temp/detection.jpg', detection)


def extract_plate_with_wpod(input_image):
    LpImg, cor = get_plate(input_image)
    mpimg.imsave("temp/crop_wpod.png", LpImg[0])
    crop = cv2.imread("temp/crop_wpod.png")
    crop = process(crop)
    crop = cv2.resize(crop, (525, 126))
    cv2.imwrite('temp/crop_wpod_final.png', crop)


def main(input_image, wpod_enabled=False):
    try:
        os.mkdir('temp')
    except:
        files = glob.glob('tmp')
        for f in files:
            os.remove(f)
    if wpod_enabled:
        extract_plate_with_wpod(input_image)
        platenum = src.filter_and_recongize_plate.get_predicted_numerals('temp/crop_wpod.png')
    else:
        extract_plate(input_image)
        platenum = src.filter_and_recongize_plate.get_predicted_numerals('temp/crop1.jpg')
    return platenum
