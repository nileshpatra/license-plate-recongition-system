import src.extract_and_recognize
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--image', help="Input image path", type= str)
    parser.add_argument('-w', '--wpod', help="Boolean to Use wpod", type= bool)
    args = parser.parse_args()
    if args.image:
        if args.wpod:
            src.extract_and_recognize.main(args.image, True)
        else:
            src.extract_and_recognize.main(args.image)
    else:
        print("Image argument is not supplied!")
