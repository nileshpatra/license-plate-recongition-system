import os
import cv2
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from sklearn.preprocessing import LabelEncoder
from keras.models import model_from_json
import numpy as np
from src import numerals_detection_model


def preprocess(image_path):
    image = cv2.imread(image_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = image / 255
    image = cv2.resize(image, (500, 224))
    return image


def get_segmented(image_path):
    image = preprocess(image_path)
    # Scales, calculates absolute values, and converts the result to 8-bit.
    plate_image = cv2.convertScaleAbs(image, alpha=(255.0))
    # convert to grayscale
    gray = cv2.cvtColor(plate_image, cv2.COLOR_BGR2GRAY)
    # Gaussian blur the image with a kernel size of 7x7
    blur = cv2.GaussianBlur(gray, (7, 7), 0)

    # Apply inverse thresh_binary with threshold value 180
    binary = cv2.threshold(blur, 180, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
    # Apply morph dilation
    kernel3 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    thre_mor = cv2.morphologyEx(binary, cv2.MORPH_DILATE, kernel3)

    # Get countour of each numeral
    def get_contours(cnts, reverse = False):
        i = 0
        boundingBoxes = [cv2.boundingRect(c) for c in cnts]
        (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes), key=lambda b: b[1][i], reverse=reverse))
        return cnts

    cont, _ = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # create a copy version "test_roi" of plat_image to draw bounding box
    test_roi = plate_image.copy()

    # Initialize a list which will be used to append charater image
    crop_characters = []

    # define standard width and height of character
    digit_w, digit_h = 30, 60

    for c in get_contours(cont):
        (x, y, w, h) = cv2.boundingRect(c)
        ratio = h / w
        if 1 <= ratio <= 3.5:  # Only select contour with defined ratio
            if h / plate_image.shape[0] >= 0.5:  # Select contour which has the height larger than 50% of the plate
                # Draw bounding box arroung digit number
                cv2.rectangle(test_roi, (x, y), (x + w, y + h), (0, 255, 0), 2)

                # Sperate number and give prediction
                curr_num = thre_mor[y:y+h, x:x+w]
                curr_num = cv2.resize(curr_num, dsize=(digit_w, digit_h))
                _, curr_num = cv2.threshold(curr_num, 220, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
                crop_characters.append(curr_num)

    print(f"[INFO] Detected {len(crop_characters)} letters")
    return crop_characters


def load_model():
    training_done = os.path.exists('model_data/NumericModelParams/Mobilenetstate.h5') and os.path.exists('model_data/NumericModelParams/Mobilenetstate.json')
    if not training_done:
        numerals_detection_model.train_numeral_detect()
    elif not os.path.exists('model_data/dataset_characters'):
        numerals_detection_model.preprocess_dataset()

    # Load model architecture, weight and labels
    json_file = open('model_data/NumericModelParams/Mobilenetstate.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    model.load_weights("model_data/NumericModelParams/Mobilenetstate.h5")
    print("[INFO] Model loaded successfully!")
    labels = LabelEncoder()
    labels.classes_ = np.load('license_character_classes.npy')
    print("[INFO] Labels loaded successfully!")
    return model, labels


def predict_from_model(image, model, labels):
    image = cv2.resize(image, (80, 80))
    image = np.stack((image,)*3, axis=-1)
    prediction = labels.inverse_transform([np.argmax(model.predict(image[np.newaxis, :]))])
    return prediction


def predict_number(crop_characters, model, labels):
    final_string = ''
    for i, character in enumerate(crop_characters):
        title = np.array2string(predict_from_model(character, model, labels))
        final_string += title.strip("'[]")
    print("Achieved result: ", final_string)
    return final_string


def get_predicted_numerals(image_path):
    crop_characters = get_segmented(image_path)
    model, labels = load_model()
    return predict_number(crop_characters, model, labels)
