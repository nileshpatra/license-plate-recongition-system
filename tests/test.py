import sys
import os
import nose
import hashlib
from keras.utils.vis_utils import plot_model
import matplotlib.pyplot as plt
import src
from src.filter_and_recongize_plate import *
from src.numerals_detection_model import preprocess_dataset
sys.path.append(os.path.abspath(os.path.join('..', 'src')))
sys.path.append(os.path.abspath(os.path.join('..', 'model_data')))


def get_md5(file_check):
    with open(file_check, "rb") as f:
        file_hash = hashlib.md5()
        while chunk := f.read(8192):
            file_hash.update(chunk)
    return file_hash.hexdigest()


def test_preprocess_dataset():
    preprocess_dataset()
    assert get_md5("license_character_classes.npy") == "124fdcf16773a469513c537cd65f4a56"


def test_get_segmented():
    crop_characters = get_segmented('tests/test_data/01-extract-numerals-from-plate.png')
    for idx, _ in enumerate(crop_characters):
        plt.imsave('test' + str(idx) + '.png', crop_characters[idx])

    assert get_md5('test0.png') == "3fe2c485437115076341ac4a31f292a5"
    assert get_md5('test1.png') == "8e13ca272a8eb8dc4db4a7ee92965bd9"
    assert get_md5('test2.png') == "f144801daf1b4cbb53e1522c5b1ccb51"
    assert get_md5('test3.png') == "d3b7eaa3fa31a06ab8a7e7a1a86eeee2"
    assert get_md5('test4.png') == "6a06660909c17e91b57b7ac40c7cd43d"
    assert get_md5('test5.png') == "1bfec57320947e95ead54d7924e3cb0a"
    assert get_md5('test6.png') == "c959b937240601f147cdcffba94d5e16"


def test_load_model():
    raise nose.SkipTest('need graphviz and pydotplus')
    model, labels = load_model()
    plot_model(model, to_file='model_plot.png', show_shapes=True, show_layer_names=True)
    assert get_md5('license_character_classes.npy') == "124fdcf16773a469513c537cd65f4a56"


def test_get_predicted_numerals():
    assert get_predicted_numerals('tests/test_data/01-extract-numerals-from-plate.png') == 'KPC1313'


def test_extract_and_recognize():
    os.system(sys.executable + " -m src.runanalysis --image tests/test_data/02-extract-dutch-license-plate.png")
    assert get_md5('temp/crop1.jpg') == "5a63b22abe688fe980264945fcabd72c"
    assert get_md5('temp/detection.jpg') == "55324c47ee205fd438f868cf632869c5"
